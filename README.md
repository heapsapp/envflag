envflag
=======

Package [envflag](https://bitbucket.org/dchapes/envflag)
is a [Go](https://golang.org/) package that
exposes environment variables for all the flags within
a [`flag.FlagSet`](https://golang.org/pkg/flag#FlagSet).

[![GoDoc](https://godoc.org/bitbucket.org/dchapes/envflag?status.png)](https://godoc.org/bitbucket.org/dchapes/envflag)


It came out of a response to a
[Reddit post](https://www.reddit.com/r/golang/comments/6fmkdc/envy_automatic_env_vars_for_all_your_flags/)
by [jalquiza](https://www.reddit.com/user/jalquiza)
about their [envy package](https://github.com/jamiealquiza/envy).

This package differs from that one in several ways:

* the usage is slightly different, it is not a drop in replacement

* it can be used with any `flag.FlagSet`, not just `flag.CommandLine`

* it tries harder (compared to the original `envy`) to avoid effecting
  the reported argument defaults or the results of functions/methods such as
  `flag.Visit` and `flag.NFlag`

* if there are any errors parsing environment variables (e.g. a string value where
  an interger value is expected) the first such encountered is returned

See the
[GoDoc documentation](https://godoc.org/bitbucket.org/dchapes/envflag)
for complete examples but basically you can just replace the call to:

```go
    flag.Parse()
```

with:

```go
    envflag.Parse("PROGNAME", true)
```

to have envirment variables such as `PROGNAME_PORT` be used for
the program's `-port` argument when no such argument is provided.

Copyright and License
---------------------

This package is Copyright 2017, Dave Chapeskie.

You are free to do with it anything you want, with the sole exception
being you cannot replicate this package and claim you wrote it.
