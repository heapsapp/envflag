package envflag_test

import (
	"flag"
	"fmt"

	"bitbucket.org/heapsapp/envflag"
)

func Example() {
	// The real example is below in main, this just runs it.
	// Unfortunately this example and it's output does not show up
	// correctly/the-same on godoc.org as it does locally :(.

	// Normally you don't need to change these package variables but
	// they help with testing. Here they are used to simulate running:
	//     env EXAMPLE_A="env A" EXAMPLE_B="env B" program -a "arg A"
	exampleEnv := map[string]string{
		"EXAMPLE_A": "env A",
		"EXAMPLE_B": "env B",
		"EXAMPLE_c": "note this isn't used",
	}
	envflag.LookupEnv = func(key string) (string, bool) {
		v, ok := exampleEnv[key]
		return v, ok
	}
	envflag.CommandLineArgs = []string{"-a", "arg A"}

	main()
	// Output:
	// a: "arg A"	true
	// b: "env B"	true
	// c: "default C"	false
}

func main() {
	// Setup your flags as normal.
	flagA := flag.String("a", "default A", "usage A")
	flagB := flag.String("b", "default B", "usage B")
	flagC := flag.String("c", "default C", "usage C")

	// Then replace your regular parse call.
	//flag.Parse()                 // <- replace this
	envflag.Parse("EXAMPLE", true) // <- with this

	wouldVisit := make(map[string]bool)
	flag.Visit(func(f *flag.Flag) { wouldVisit[f.Name] = true })
	// Values come from args, environment, or defaults (in that order).
	// Flags set from either args or the environment are indistinguishable
	// from each other but are both distinguishable from defaulted flags.
	fmt.Printf("a: %q\t%t\n", *flagA, wouldVisit["a"])
	fmt.Printf("b: %q\t%t\n", *flagB, wouldVisit["b"])
	fmt.Printf("c: %q\t%t\n", *flagC, wouldVisit["c"])
}
