package envflag

import (
	"bytes"
	"flag"
	"sort"
	"strconv"
	"strings"
	"testing"
)

type testEnv map[string]string

func (e testEnv) LookupEnv(key string) (string, bool) {
	v, ok := e[key]
	return v, ok
}

// overrideLookup is test utility to override LookupEnv
// and return a function to reset the override.
//
// Can be used with defer as:
//     defer e.override()()
//
// NB: The first () does the override and the required
//     trailing/extra () defers the reset call.
func (e testEnv) override() func() {
	old := LookupEnv
	LookupEnv = e.LookupEnv
	return func() { LookupEnv = old }
}

func dumpFlagVals(t *testing.T, eset *FlagSet) {
	var keys sort.StringSlice
	vals := make(map[string]string)
	eset.VisitAll(func(f *flag.Flag) {
		keys = append(keys, f.Name)
		vals[f.Name] = f.Value.String()
	})
	keys.Sort()
	for _, k := range keys {
		t.Logf("%q = %q", k, vals[k])
	}
}

// TODO: more complete testing of all corner cases

func TestBasic(t *testing.T) {
	fset := flag.NewFlagSet("testbasic", flag.ContinueOnError)
	eset := New(fset, "BASIC", true)
	eset.Bool("b1", false, "a bool")
	eset.Bool("b2", false, "a second bool")
	eset.Int("int1", 1, "an integer")
	eset.Int("int2", 2, "a second integer")
	eset.String("str1", "one", "a string")
	eset.String("str2", "two", "a second string")
	env := testEnv{
		"BASIC_B1":   "true",
		"BASIC_B2":   "false",
		"BASIC_INT1": "42",
		"BASIC_INT2": "8080",
		"BASIC_STR1": "changed",
		"BASIC_STR2": "foobar",
	}
	defer env.override()()
	var buf bytes.Buffer
	eset.SetOutput(&buf)
	expect := [...]struct {
		name    string
		varname string
		val     string
	}{
		{"b1", "BASIC_B1", "true"},        // set by env
		{"b2", "BASIC_B2", "true"},        // set by args
		{"int1", "BASIC_INT1", "42"},      // set by env
		{"int2", "BASIC_INT2", "100"},     // set by args, not env
		{"str1", "BASIC_STR1", "changed"}, // set by env
		{"str2", "BASIC_STR2", "good"},    // set by args, not env
	}
	// Do the Parse twice. Calling Parse a second time with the
	// same arguements shouldn't change anything, in particular it should
	// not adjust the usage strings a second time.
	for i := 0; i < 2; i++ {
		buf.Reset()
		err := eset.Parse([]string{"-b2", "-int2", "100", "-str2", "good"})
		if err != nil {
			t.Fatal(i, "unexpected Parse error:", err)
		}
		eset.Usage()
		for _, ex := range expect {
			f := eset.Lookup(ex.name)
			cnt := strings.Count(f.Usage, "["+ex.varname+"]")
			if cnt != 1 {
				t.Errorf("usage for -%s contains %d references to %s, want 1",
					ex.name, cnt, ex.varname)
			}
			if g := f.Value.String(); g != ex.val {
				t.Errorf("value of %s gave %q, want %q",
					ex.name, g, ex.val)
			}
		}
		if t.Failed() {
			t.Logf("output %d: %s", i, buf.Bytes())
			dumpFlagVals(t, eset)
		}
	}
}

func TestParseError(t *testing.T) {
	env := testEnv{
		"PE_INT":   "not an int",
		"PE_FLOAT": "not a float",
	}
	defer env.override()()
	fset := flag.NewFlagSet("testerror", flag.ContinueOnError)
	eset := New(fset, "PE", false)
	_ = eset.Int("int", 42, "an int")
	_ = eset.Float64("float", 3.14, "a float")
	err := eset.Parse(nil)
	if err == nil {
		t.Fatal("expected an error, got none")
	}
	switch enverr := err.(type) {
	case *ParseError:
		switch suberr := enverr.Err.(type) {
		case *strconv.NumError:
			t.Log("got error as expected:", err)
		default:
			t.Errorf("unexpected ParseError.Err type: %#v", suberr)
		}
	default:
		t.Errorf("unexpected err type: %#v", enverr)
	}
}
