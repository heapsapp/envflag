// Package envflag exposes environment variables for all the flags within
// a flag.FlagSet.
package envflag

import (
	"flag"
	"os"
	"strings"
)

var (
	// LookupEnv is the function used to lookup environment variables.
	LookupEnv = os.LookupEnv

	// CommandLineArgs are the arguments for the Parse function.
	CommandLineArgs = os.Args[1:]
)

// FlagSet is a wrapper around flag.FlagSet.
// It optionally adds environment variable information to Usage and
// adjusts the Parse method to lookup environment variables before
// parsing the arguments.
type FlagSet struct {
	*flag.FlagSet
	prefix      string
	updateUsage bool
}

// New creates a new wrapper around the provided flag set.
// The prefix argument is used to determine the environment variable names.
// If updateUsage is true then each flag's usage string is appended with
// the matching environment variable name before parsing.
func New(fset *flag.FlagSet, prefix string, updateUsage bool) *FlagSet {
	return &FlagSet{
		FlagSet:     fset,
		prefix:      prefix,
		updateUsage: updateUsage,
	}
}

// Parse parses the command-line flags using environment
// variables based on prefix and flag.CommandLine.
// It is convience to wrapping flag.CommandLine.
func Parse(prefix string, updateUsage bool) error {
	e := New(flag.CommandLine, prefix, updateUsage)
	return e.Parse(CommandLineArgs)
}

// A ParseError records a failed environment variable parse.
type ParseError struct {
	EnvName string // name of environment variable being parsed
	Err     error  // underlying error
}

func (e *ParseError) Error() string {
	return "parsing $" + e.EnvName + ": " + e.Err.Error()
}

// Parse uses the prefix to lookup and parse environment variables
// to change the FlagSet's defaults and optionally adjust the usage strings.
// It finishes by calling the underlying FlagSet.Parse method.
// If there is an error parsing any of the environment variables parsing
// continues to completion and the first such error is returned.
//
// Note that if there is an error parsing the provided arguments the
// underlying FlagSet's ErrorHandling may panic or exit.
func (e *FlagSet) Parse(args []string) error {
	var firstErr error
	e.FlagSet.VisitAll(func(f *flag.Flag) {
		// Create an env var name based on the supplied prefix.
		envVar := e.prefix + "_" + strings.ToUpper(f.Name)
		envVar = strings.Replace(envVar, "-", "_", -1)

		// Update the Flag.Value if the env var is set, even if empty.
		// This effectively changes the defaults for flag.FlagSet.Parse.
		if val, ok := LookupEnv(envVar); ok {
			// Could use f.Value.Set to not effect Visit status
			if err := e.FlagSet.Set(f.Name, val); err != nil && firstErr == nil {
				firstErr = &ParseError{envVar, err}
			}
		}

		if e.updateUsage {
			// Append the env var to the Flag.Usage field.
			f.Usage += " [" + envVar + "]"
		}
	})
	e.updateUsage = false // Don't do this again.
	if err := e.FlagSet.Parse(args); err != nil && firstErr == nil {
		firstErr = err
	}
	return firstErr
}
