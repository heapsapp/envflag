package envflag_test

import (
	"flag"
	"os"

	"bitbucket.org/heapsapp/envflag"
)

func Example_usage() {
	fset := flag.NewFlagSet("example", flag.ContinueOnError)
	fset.String("a", "default A", "usage A")
	fset.String("b", "default B", "usage B")
	fset.String("c", "default C", "usage C")
	fset.SetOutput(os.Stdout)

	eset := envflag.New(fset, "EXAMPLE", true)
	eset.Parse([]string{"-h"})

	// Note that the returned envflag.FlagSet embeds flag.FlagSet
	// so alternatively, after creating `eset` it could
	// be used in place of `fset` in the above example.

	// Output:
	// Usage of example:
	//   -a string
	//     	usage A [EXAMPLE_A] (default "default A")
	//   -b string
	//     	usage B [EXAMPLE_B] (default "default B")
	//   -c string
	//     	usage C [EXAMPLE_C] (default "default C")
}
